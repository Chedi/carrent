//
// server.hpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER_HPP
#define HTTP_SERVER_HPP

#include <string>
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

#include "Connection.h"
#include "RequestHandler.h"
#include "ConnectionManager.h"

namespace Http   {
namespace Server {

class server : private boost::noncopyable {
public:
  explicit server(const std::string& address, const std::string& port, const std::string& doc_root);
  void run();

private:
  void start_accept();
  void handle_accept(const boost::system::error_code& e);
  void handle_stop();

  connection_ptr                 new_connection_;
  request_handler                request_handler_;
  connection_manager             connection_manager_;
  boost::asio::io_service        io_service_;
  boost::asio::signal_set        signals_;
  boost::asio::ip::tcp::acceptor acceptor_;
};

}}

#endif // HTTP_SERVER_HPP
#include <Dao/Client/ClientDAO.h>
#include <Dao/Common/PersonDAO.h>

namespace DAO {

using std::to_string          ;
using boost::fusion::at_key   ;
using boost::fusion::make_pair;

template<> const string ClientDAO::INSERT_QUERY             = "INSERT INTO clients(driver_licence, age, person_id) VALUES ('$driver_licence', $age, $person_id) returning id";
template<> const string ClientDAO::UPDATE_QUERY             = "UPDATE clients SET driver_licence = '$driver_licence', age = $age, person_id = $person_id WHERE id = $id";
template<> const string ClientDAO::DELETE_QUERY             = "DELETE FROM clients WHERE id = $id";
template<> const string ClientDAO::GET_ALL_QUERY            = "SELECT u.* FROM clients u";
template<> const string ClientDAO::SERIALIZATION            = "{\"id\" : \"$id\", \"driver_licence\" : \"$driver_licence\", \"age\" : $age, \"person\" : $person_id}";
template<> const string ClientDAO::GET_BY_ID_QUERY          = "SELECT u.* FROM clients u WHERE u.id = $id";
template<> const string ClientDAO::GET_BY_ATTRIBUTES_QUERY  = "SELECT u.* FROM clients u WHERE u.driver_licence = '$driver_licence' AND u.age = $age AND person_id = $person_id" ;
template<> const string ClientDAO::GET_TABLE_CREATION_QUERY = "CREATE TABLE clients(id SERIAL PRIMARY KEY, driver_licence VARCHAR NOT NULL, age INT NOT NULL, person_id INT NOT NULL REFERENCES persons(id))";

template<>
ClientDAO* ClientDAO::parseResult(query_row row){
	auto person       = make_shared<PersonDAO>(row["person_id"].as<int>());
	auto person_model = person->getModel();
	auto result = new ClientDAO(
		make_shared<Client>(
				person_model                                           ,
				make_shared<string>(row["driver_licence"].as<string>()), 
				                    row["age"           ].as<int>   ()), 
		row["id"].as<int>());
	at_key<boost::mpl::pair<PersonDAO, int_<0>>>(result->dependencies) = person;
	return result;
}

template<>
void ClientDAO::initializeFunctors() {
	serializers_map = {
		{"id"            , DAOSerializer{[this](){ return to_string(id)               ; }}},
		{"age"           , DAOSerializer{[this](){ return to_string(model->getAge  ()); }}},
		{"driver_licence", DAOSerializer{[this](){ return *(model->getDriverLicence()); }}},
		{"person_id", DAOSerializer{
			[this](){ return synchronizeDependency<boost::mpl::pair<PersonDAO, int_<0>>>(false); },
			[this](){ return synchronizeDependency<boost::mpl::pair<PersonDAO, int_<0>>>(true ); }
		}}};

	modelDependencies = ClientDAOModelDependencies(
		make_pair<boost::mpl::pair<PersonDAO, int_<0>>>(
			[this](){ return model->getPerson() ;}
	));
}

}